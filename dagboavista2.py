from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.providers.google.cloud.operators.dataproc import (
    DataprocCreateClusterOperator,
    DataprocDeleteClusterOperator,
    DataprocSubmitPySparkJobOperator
)

# Default arguments for the DAG
default_args = {
    'owner': 'airflow',
    'start_date': days_ago(1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

# Define the DAG
dag = DAG(
    'pyspark_dataproc_example',
    default_args=default_args,
    description='Run a PySpark job on GCP Dataproc',
    schedule_interval=timedelta(days=1),
)

# Task to create a Dataproc cluster
create_cluster = DataprocCreateClusterOperator(
    task_id='create_dataproc_cluster',
    project_id='your-gcp-project-id',
    cluster_config={
        'master_config': {
            'num_instances': 1,
            'machine_type_uri': 'n1-standard-2'
        },
        'worker_config': {
            'num_instances': 2,
            'machine_type_uri': 'n1-standard-2'
        }
    },
    region='us-central1',
    dag=dag,
)

# Task to submit a PySpark job to the cluster
submit_pyspark_job = DataprocSubmitPySparkJobOperator(
    task_id='submit_pyspark_job',
    main='gs://your-bucket/path/to/your-pyspark-script.py',
    cluster_name='your-cluster-name',
    region='us-central1',
    dag=dag,
)

# Task to delete the Dataproc cluster
delete_cluster = DataprocDeleteClusterOperator(
    task_id='delete_dataproc_cluster',
    project_id='your-gcp-project-id',
    cluster_name='your-cluster-name',
    region='us-central1',
    dag=dag,
)

# Define DAG dependencies
create_cluster >> submit_pyspark_job >> delete_cluster

#Cria um cluster Dataproc na GCP.
#Submete um trabalho PySpark ao cluster.
#Exclui o cluster após a conclusão do trabalho.

#Certifique-se de substituir os marcadores de espaço reservado (como 
#your-gcp-project-id, your-cluster-name, e o caminho para o script PySpark em
# #gs://your-bucket/path/to/your-pyspark-script.py) com os valores reais adequados 
#ao seu ambiente e necessidades.