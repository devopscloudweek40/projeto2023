 Vamos desenvolver o nosso primeiro workflow
# É necessário colocar este arquivo dentro da pasta de DAGS do AIRFLOW


# Verificar no arquivo de configuração do AIRFLOW o path dos DAGS
# dags_folder = /root/airflow/dags

# Módulos do Airflow
from datetime import datetime, timedelta  
from airflow import DAG
from airflow.operators.bash_operator import BashOperator


# Vamos criar um objeto DAG
# Para isso, devemos especificar alguns parâmetros básicos, tais como: 
# Quando se tornará ativo 
# Quais os intervalos que queremos que execute 
# Quantas tentativas devem ser feitas caso alguma de suas tarefas falhe 
# Vamos definir esses parâmetros

default_args = {
    'owner': 'airflow',   #proprietario da DAG
    'depends_on_past': False,  #Dependencia de outra execução se for "F" não depende
    # Exemplo: Inicia em 20 de Janeiro de 2021
    'start_date': datetime(2021, 1, 20), #A eecução de dará depois desta data
    'email': ['airflow@example.com'], #Notificação por email
    'email_on_failure': False, #Enviar email se falhar se "F" não envia
    'email_on_retry': False,   #Enviar email nova tentativa "F" não envia
    'retries': 1,    # Em caso de erros, tente rodar novamente apenas 1 vez
    'retry_delay': timedelta(seconds=30), # Tente novamente após 30 segundos depois do erro
}



# Exemplo de definição de schedule 
# .---------------- minuto (0 - 59)
# |  .------------- hora (0 - 23)
# |  |  .---------- dia do mês (1 - 31)
# |  |  |  .------- mês (1 - 12) 
# |  |  |  |  .---- dia da semana (0 - 6) (Domingo=0 or 7)
# |  |  |  |  |
# *  *  *  *  * (nome do usuário que vai executar o comando)

#With padrão definição das DAGs

with DAG(
    dag_id='meu_dag',  # O identificador único para a DAG
    default_args=default_args,  # Argumentos padrão definidos anteriormente que serão aplicados a todas as tarefas na DAG
    schedule_interval=None,  # Define a DAG para ser acionada manualmente ou por meio de um trigger externo
    #schedule_interval='*/15 * * * *',  # Aqui é o local comum para definir o schedule_interval
    tags=['exemplo'],  # Lista de tags que ajudam a categorizar e filtrar DAGs na UI do Airflow
    ) as dag:  # Início do bloco de contexto da DAG, 'dag' é a instância da DAG criada


    # Vamos Definir a nossa Primeira Tarefa 
    t1 = BashOperator(bash_command="touch ~/meu_arquivo_01.txt", task_id="criar_arquivo")

    # Vamos definir a nossa segunda tarefa
    t2 = BashOperator(bash_command="mv ~/meu_arquivo_01.txt ~/meu_arquivo_01_MUDOU.txt", task_id="mudar_nome_do_arquivo")    

    # Configurar a tarefa T2 para ser dependente da tarefa T1
    t1 >> t2 


# Parar o WebServer 
# Parar o Scheduler
# Iniciar o WebServer 
# Iniciar o Scheduler


# Vamos Acessar Interface WEB do Airflow e procurar o nosso DAG 
# Vamos clicar à sua esquerda, para que seja ativado. 
# Vamos verificar a visualização do DAG para confirmar se o dag foi importado corretamente (clicar no nome do dag)


# Vamos confirmar que o arquivo não existem originalmente 
ls -lrth ~/meu_arquivo_01.txt
ls -lrth ~/meu_arquivo_01_MUDOU.txt
ls -lrth ~/


# Por fim, no lado direito, clique no botão PLAY para disparar o DAG manualmente
# Clicar no DAG nos permite ver o status das últimas execuções. 
# Informar parâmetros adicionais antes de disparar a execução


# Após execução: Triggered meu_dag, it should start any moment now. 
# Clicar no horário de execução do DAG para verificar os detalhes
# Confirmar se as tarefas estão "verdes", mostrando que foram executadas com sucesso


# Vamos confirmar se os arquivos foram criados corretamente 
ls -lrth ~/meu_arquivo_01.txt
ls -lrth ~/meu_arquivo_01_MUDOU.txt
ls -lrth ~/


# Se clicarmos em Graph View, devemos ver uma representação gráfica de nosso DAG - junto com os códigos de cores que indicam o status de execução de cada tarefa.
# Vamos também se o DAG executou com sucesso olhando o nosso diretório inicial

