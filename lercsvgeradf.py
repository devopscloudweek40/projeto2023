import pandas as pd

# Substitua 'seu_arquivo.csv' pelo caminho do seu arquivo CSV
caminho_arquivo_csv = 'C:/GIOVANNINI/0000-A-A-Porto-GCP/Arquivo_csv_outros/pr_gasolina_br.csv'

try:
    # Lê o arquivo CSV em um DataFrame do pandas
    #df = pd.read_csv(caminho_arquivo_csv, usecols=colunas_desejadas)
    dfmeu = pd.read_csv(caminho_arquivo_csv, delimiter=';')
    # Realize qualquer análise que você precise no DataFrame df aqui

    #imprime data frame
    print(dfmeu)

    dfmeu.info()

except FileNotFoundError:
    print(f"O arquivo '{caminho_arquivo_csv}' não foi encontrado.")
except Exception as e:
    print(f"Ocorreu um erro: {str(e)}")
