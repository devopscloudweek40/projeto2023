from datetime import datetime, timedelta  # Importando as funções necessárias dos módulos datetime
from airflow import DAG  # Importando a classe DAG do módulo airflow

# Definição dos argumentos padrão
default_args = {
    'owner': 'airflow',  # Define o proprietário da DAG, geralmente quem a criou
    'depends_on_past': False,  # Define se uma tarefa depende da execução da tarefa no intervalo de execução anterior
    'start_date': datetime(2021, 1, 1),  # A data de início da programação da DAG
    'email': ['alert@example.com'],  # Lista de e-mails para alertas
    'email_on_failure': True,  # Se True, Airflow envia um e-mail em caso de falha da tarefa
    'email_on_retry': False,  # Se True, Airflow envia um e-mail a cada nova tentativa de execução da tarefa
    'retries': 1,  # Número de tentativas em caso de falhas
    'retry_delay': timedelta(minutes=5),  # Tempo de espera antes de realizar uma nova tentativa
    'catchup': False  # Se False, evita a execução em atraso para datas anteriores à 'start_date'
}

# Criação da DAG usando os argumentos padrão
dag = DAG(
    'example_dag',  # Nome único da DAG
    default_args=default_args,  # Aplica os argumentos padrões definidos acima a todas as tarefas da DAG
    description='A simple tutorial DAG',  # Descrição da DAG
    schedule_interval=timedelta(days=1),  # Intervalo de programação da execução da DAG
)
