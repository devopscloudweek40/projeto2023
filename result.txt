Primeiras linhas do DataFrame:
   Unnamed: 0 DATA INICIAL  DATA FINAL  ... COEF DE VAR DISTRIB MES   ANO
0           0   09/05/2004  15/05/2004  ...               0.133   5  2004
1           1   09/05/2004  15/05/2004  ...               0.115   5  2004
2           2   09/05/2004  15/05/2004  ...               0.098   5  2004
3           3   09/05/2004  15/05/2004  ...               0.143   5  2004
4           4   09/05/2004  15/05/2004  ...               0.082   5  2004

[5 rows x 21 columns]

Informacoes Gerais:
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 106823 entries, 0 to 106822
Data columns (total 21 columns):
 #   Column                        Non-Null Count   Dtype  
---  ------                        --------------   -----  
 0   Unnamed: 0                    106823 non-null  int64  
 1   DATA INICIAL                  106823 non-null  object 
 2   DATA FINAL                    106823 non-null  object 
 3   REGIAO                        106823 non-null  object 
 4   ESTADO                        106823 non-null  object 
 5   PRODUTO                       106823 non-null  object 
 6   NUMERO DE POSTOS PESQUISADOS  106823 non-null  int64  
 7   UNIDADE DE MEDIDA             106823 non-null  object 
 8   PRECO MEDIO REVENDA           106823 non-null  float64
 9   DESVIO PADR├O REVENDA         106823 non-null  float64
 10  PRECO MINIMO REVENDA          106823 non-null  float64
 11  PRECO MAXIMO REVENDA          106823 non-null  float64
 12  MARGEM DIA REVENDA            106823 non-null  object 
 13  COEF DE VARIACAO REVENDA      106823 non-null  float64
 14  PRECO MEDIO DISTRIBUICAO      106823 non-null  object 
 15  DESVIO PADRAO DISTRIBUIDORA   106823 non-null  object 
 16  PRECO MINI DISTRIBUIDORA      106823 non-null  object 
 17  PRECO MAX DISTRIB             106823 non-null  object 
 18  COEF DE VAR DISTRIB           106823 non-null  object 
 19  MES                           106823 non-null  int64  
 20  ANO                           106823 non-null  int64  
dtypes: float64(5), int64(4), object(12)
memory usage: 17.1+ MB

Estatisticas Basicas das Colunas Numericas:
         Unnamed: 0  NUMERO DE POSTOS PESQUISADOS  ...            MES            ANO
count  106823.00000                 106823.000000  ...  106823.000000  106823.000000
mean    53411.00000                    233.770976  ...       6.484006    2011.802271
std     30837.28824                    403.282519  ...       3.443391       4.391530
min         0.00000                      1.000000  ...       1.000000    2004.000000
25%     26705.50000                     42.000000  ...       4.000000    2008.000000
50%     53411.00000                    104.000000  ...       6.000000    2012.000000
75%     80116.50000                    243.000000  ...       9.000000    2016.000000
max    106822.00000                   4167.000000  ...      12.000000    2019.000000

[8 rows x 9 columns]

Estatisticas Basicas das Colunas Float64:
       PRECO MEDIO REVENDA  ...  COEF DE VARIACAO REVENDA
count        106823.000000  ...             106823.000000
mean             10.870780  ...                  0.045418
std              17.752533  ...                  0.025066
min               0.766000  ...                  0.000000
25%               2.072000  ...                  0.029000
50%               2.718000  ...                  0.041000
75%               3.752000  ...                  0.058000
max              99.357000  ...                  0.395000

[8 rows x 5 columns]

Estatisticas Basicas das Colunas Object:
       DATA INICIAL  DATA FINAL  ... PRECO MAX DISTRIB COEF DE VAR DISTRIB
count        106823      106823  ...            106823              106823
unique          785         785  ...             22500                 397
top      19/07/2015  25/07/2015  ...                 -                   -
freq            154         154  ...              3400                3400

[4 rows x 12 columns]

Contagem de Valores Ausentes por Coluna:
Unnamed: 0                      0
DATA INICIAL                    0
DATA FINAL                      0
REGIAO                          0
ESTADO                          0
PRODUTO                         0
NUMERO DE POSTOS PESQUISADOS    0
UNIDADE DE MEDIDA               0
PRECO MEDIO REVENDA             0
DESVIO PADR├O REVENDA           0
PRECO MINIMO REVENDA            0
PRECO MAXIMO REVENDA            0
MARGEM DIA REVENDA              0
COEF DE VARIACAO REVENDA        0
PRECO MEDIO DISTRIBUICAO        0
DESVIO PADRAO DISTRIBUIDORA     0
PRECO MINI DISTRIBUIDORA        0
PRECO MAX DISTRIB               0
COEF DE VAR DISTRIB             0
MES                             0
ANO                             0
dtype: int64
