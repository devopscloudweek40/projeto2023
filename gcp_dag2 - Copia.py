####################################################################################
## AUTOR: Marco
## DATA: 12/09/2022
## DESCRICAO: DAG QUE REALIZA CARGA DE ARQUIVOS CSV NO BIGQUERY, GERA UMA TABELA FILTRADA E MOVE OS ARQUIVOS FONTES.
## ALTERADO ARQUIVO csv
####################################################################################

from datetime import datetime, timedelta  
import airflow
from airflow import DAG
from airflow.operators import bash_operator
import airflow.providers.google.cloud.operators.bigquery
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator
from airflow.providers.google.cloud.transfers.gcs_to_gcs import GCSToGCSOperator

from airflow.providers.google.cloud.operators.bigquery import (
    BigQueryExecuteQueryOperator,
)

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2021, 1, 20),
    'email': ['mag408952@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
}

with DAG(    
    dag_id='gcp_dag',
	is_paused_upon_creation=False,
    default_args=default_args,
	schedule_interval=None,   #vai se executada manualmente
	catchup=False,    #não deixa executar DAG da data inicio até data atual
    tags=['gcp','dados-com-giova','big-query'],
) as dag:    

	t1_load_csv_bq = GCSToBigQueryOperator(
    task_id='carrega_csv_tb_raw_applestore',
    bucket='bucket_giova',
#   source_objects='bucket_giova/landing_zone/*.csv', copia todos os arquivo .csv da
    source_objects='bucket_giova/landing_zone/AppleStore.csv',
	field_delimiter=',',
	skip_leading_rows=1,
    destination_project_dataset_table=f"meudataset.tb_bronze_applestore", 
    schema_fields=[
    {'name': 'id', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'track_name', 'type': 'STRING', 'mode': 'NULLABLE'},
    {'name': 'size_bytes', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'currency', 'type': 'STRING', 'mode': 'NULLABLE'},
    {'name': 'price', 'type': 'FLOAT', 'mode': 'NULLABLE'},
    {'name': 'rating_count_tot', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'rating_count_ver', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'user_rating', 'type': 'FLOAT', 'mode': 'NULLABLE'},
    {'name': 'user_rating_ver', 'type': 'FLOAT', 'mode': 'NULLABLE'},
    {'name': 'ver', 'type': 'STRING', 'mode': 'NULLABLE'},
    {'name': 'cont_rating', 'type': 'STRING', 'mode': 'NULLABLE'},
    {'name': 'prime_genre', 'type': 'STRING', 'mode': 'NULLABLE'},
    {'name': 'sup_devices.num', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'ipadSc_urls.num', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'lang.num', 'type': 'INTEGER', 'mode': 'NULLABLE'},
    {'name': 'vpp_lic', 'type': 'BOOLEAN', 'mode': 'NULLABLE'},
],
    write_disposition='WRITE_TRUNCATE',
    dag=dag,
)

	t2_creating_table_from_query = BigQueryExecuteQueryOperator(
    task_id="criando_tabela_bigquery",
    sql="""
    SELECT
      *
    FROM
      `data-dev-403711.meudataset.tb_prata_applestore`
	WHERE name != "Evernote" 
    """,
    destination_dataset_table=f"data-dev-403711.meudataset.tb_ouro_applestore",
    write_disposition="WRITE_TRUNCATE",
    gcp_conn_id="cnx-composergiova",   #conexão com biguery video do Caio
    use_legacy_sql=False,
)

	t3_move_src_files_to_bkp = GCSToGCSOperator(
    task_id="move_arquivo_para_bkp",
    source_bucket='meu-bucket-gcs',
    source_object='/landing_zone/AppleStore.csv',
    destination_bucket='bucket_giova',
    destination_object='/bkp/',
    move_object=True,
)

	t1_load_csv_bq >> t2_creating_table_from_query >> t3_move_src_files_to_bkp


