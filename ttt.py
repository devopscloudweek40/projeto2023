from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import subprocess

# Função que executa o script Python
def run_pyspark_script():
    # Executa o script PySpark
    subprocess.check_call(["python", "caminho/para/tttttt.py"])

# Argumentos padrões da DAG
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 1, 1),
    'email': ['email@exemplo.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

# Definindo a DAG
dag = DAG(
    'pyspark_script_dag',
    default_args=default_args,
    description='Uma DAG para executar um script PySpark',
    schedule_interval=timedelta(days=1),
)

# Definindo a tarefa
run_script = PythonOperator(
    task_id='run_pyspark_script',
    python_callable=run_pyspark_script,
    dag=dag,
)

# Configuração da ordem das tarefas
run_script
