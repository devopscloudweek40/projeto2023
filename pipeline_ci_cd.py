stages:
  - lint
  - test
  - deploy

lint-job:
  stage: lint
  script:
    - echo "Linting dag.py..."
    - flake8 dag.py

test-job:
  stage: test
  script:
    - echo "Testing dag.py..."
    - pytest

deploy-job:
  stage: deploy
  script:
    - echo "Deploying DAG..."
    # Adicione comandos específicos para deploy do DAG aqui
