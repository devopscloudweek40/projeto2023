import pandas as pd

# Substitua 'seu_arquivo.csv' pelo caminho do seu arquivo CSV
#caminho_arquivo_csv = 'C:/GIOVANNINI/0000-A-A-Porto-GCP/Arquivo_csv_outros/AppleStore.csv'
caminho_arquivo_csv = 'C:/GIOVANNINI/0000-A-A-Porto-GCP/Arquivo_csv_outros/AppleStore1.csv'
# Colunas que você deseja analisar
colunas_desejadas = ["id", "track_name", "size_bytes", "currency", "price", "rating_count_tot",
                     "rating_count_ver", "user_rating", "user_rating_ver", "ver", "cont_rating",
                     "prime_genre", "sup_devices_num", "ipadSc_urls_num", "lang_num", "vpp_lic"]

try:
    # Lê o arquivo CSV em um DataFrame do pandas
    df = pd.read_csv(caminho_arquivo_csv, delimiter=',', header=0, encoding="utf-8")

    # Seleciona apenas as colunas desejadas
    df = df[colunas_desejadas]

    # Realize qualquer análise que você precise no DataFrame df aqui

    # Exemplo: Imprimir as primeiras 5 linhas do DataFrame
    print(df.head())

except FileNotFoundError:
    print(f"O arquivo '{caminho_arquivo_csv}' não foi encontrado.")
except Exception as e:
    print(f"Ocorreu um erro: {str(e)}")
