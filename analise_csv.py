import pandas as pd

# Substitua isso pelo caminho do seu arquivo CSV
caminho_arquivo_csv = 'C:/GIOVANNINI/0000-A-A-Porto-GCP/Arquivo_csv_outros/pr_gasolina_br_total.csv'

# Carregar o arquivo CSV em um DataFrame
df = pd.read_csv(caminho_arquivo_csv, delimiter=';')

# Exibir as primeiras linhas para ter uma ideia do conjunto de dados
print("Primeiras linhas do DataFrame:")
print(df.head())

# Informações gerais sobre o DataFrame
print("\nInformacoes Gerais:")
df.info()

# Estatísticas básicas para colunas numéricas
print("\nEstatisticas Basicas das Colunas Numericas:")
print(df.describe())

# Estatísticas para colunas do tipo float
print("\nEstatisticas Basicas das Colunas Float64:")
print(df.select_dtypes(include=['float64']).describe())

# Estatísticas para colunas do tipo object
print("\nEstatisticas Basicas das Colunas Object:")
print(df.select_dtypes(include=['object']).describe())

# Contagem de valores ausentes em cada coluna
print("\nContagem de Valores Ausentes por Coluna:")
print(df.isnull().sum())

#Contar Valores Únicos em Uma Coluna Específica:
unique_count = df['REGIAO'].nunique()

#Contar Valores Únicos em Todo o DataFrame:
unique_counts = df.nunique()

#verificar diretamento os valore vazio
df.isnull().sum()

# Análise de uma coluna específica (substitua 'NomeDaColuna' pelo nome real da coluna)
# print("\nAnálise da Coluna 'NomeDaColuna':")
# print(df['NomeDaColuna'].value_counts())
